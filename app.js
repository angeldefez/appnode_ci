const express = require("express");
const app = express();

app.get("/", (request, response) => {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.end("Hello world with nodejs. Aquí con el Lucas a tope!... y Ángel");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);
